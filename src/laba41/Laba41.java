/*
 Создать статический метод, который будет иметь два целочисленных параметра a и b,
и в качестве своего значения возвращать случайное целое число из отрезка [a;b].
C помощью данного метода заполнить массив из 20 целых чисел и вывести его на экран.
 */
package laba41;

import java.util.Scanner;

/**
 *
 * @author Nastya
 */
public class Laba41 {

   //случайное число из интервала от а до б
    public static int rando(int a, int b) {

        if (a > b) {
            return (int) (Math.random() * (b - a - 1 ) + (a + 1));
        } else {
            return (int) (Math.random() * (a - b - 1 ) + (b + 1));
        }

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] mas;
        mas = new int[20];
        int a = 0;
        int b = 0;
        //проверка исключений
        try {
            System.out.println("Введите a");
            a = sc.nextInt();
            System.out.println("Введите b");
            b = sc.nextInt();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода ");
        }
        //инициализация и вывод
        for (int i = 0; i < 20; i++) {
            mas[i] = rando(a, b);
            System.out.print(mas[i] + " ");
        }
    }
}