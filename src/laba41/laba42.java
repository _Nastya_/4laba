/*
 * Создать метод, который будет выводить указанный массив на экран в строку.
С помощью созданного метода и метода из предыдущей задачи заполнить 5 массивов из
10 элементов каждый случайными числами и вывести все 5 массивов на экран,
каждый на отдельной строке.
 */
package laba41;

import java.util.Scanner;
import static laba41.Laba41.rando;

/**
 *
 * @author Nastya
 */
public class laba42 {
    

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] mas;
        mas = new int[10];
        int a = 0;
        int b = 0;
        //выводит 5 раз массив
        for (int i = 0; i < 5; i++) {
            //проверка исключений
            try {
                System.out.println("Введите a");
                a = sc.nextInt();
                System.out.println("Введите b");
                b = sc.nextInt();
            } catch (Exception ex) {
                System.out.println("Ошибка ввода ");
            }
            //инициализация и вывод
            for (int j = 0; j < 10; j++) {
                mas[j] = rando(a, b);
                System.out.print(mas[j] + " ");
            }
            System.out.println();
        }
    }
}
