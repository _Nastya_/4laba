/*
 *Создать метод, который будет сортировать указанный массив по возрастанию любым
известным вам способом.
 */
package laba41;

import java.util.Arrays;
import java.util.Scanner;
import static laba41.Laba41.rando;

/**
 *
 * @author Nastya
 */
public class laba43 {
    public static int[] Sort(int[] mass) {
        //из ссылки в строку
        System.out.println("до " + Arrays.toString(mass));
        for (int i = 0; i < mass.length; i++) {
            //Первый элемент является минимальным
            int min = mass[i];
            int min_i = i; 
            for (int j = i + 1; j < mass.length; j++) {
                //Если находим, запоминаем его индекс
                if (mass[j] < min) {
                    min = mass[j];
                    min_i = j;
                }
            }
            //Если элемент меньший, чем есть, меняем их местами
            if (i != min_i) {
                int k = mass[i];
                mass[i] = mass[min_i];
                mass[min_i] = k;
            }
        }
        return mass;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] mas;
        mas = new int[20];
        int a = 0;
        int b = 0;
        //проверка исключений
        try {
            System.out.println("Введите a");
            a = sc.nextInt();
            System.out.println("Введите b");
            b = sc.nextInt();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода ");
        }
        //инициализация и вывод
        for (int i = 0; i < 20; i++) {
            mas[i] = rando(a, b);
        }
        System.out.println();
        System.out.print(Arrays.toString(Sort(mas)));
    }
}

